// Blindwall api version 2
var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var sqlite3 = require('sqlite3').verbose();
var path = require('path');
var waterfall = require('async-waterfall');
//
// Alle endpoint behalve /apiv2/login require X-Access-Token
//
// Ook kan (weg laten van newRegExp):
//      router.all(/[^(\/login)]/, function (req, res, next)
// Let op ! zonder ' (quotes)
//
router.all(new RegExp("[^(\/login)]"), function (req, res, next) {

    // Zonder reguliere expressie een if gebruiken om router eruit
    // te filteren:
    // if( req.url.indexOf('/login') > -1) {
    //     return next();
    // }

    // For all the others
    var token = (req.header('X-Access-Token')) || '';
    if (token) {
        try {
            var decoded = jwt.decode(token, req.app.get('secretkey'));

            // Check if token is from known user
            // to do: db lookup
            var userName = req.app.get('username');

            if (decoded.iss == userName) {
                req.app.set("userid", decoded.iss);
                console.log("Userid: " + req.app.get('userid'));
                return next();
            } else {
                res.status(401);
                res.json({
                    "status": 401,
                    "message": "unknown userid, bye"
                });
            }
        } catch (err) {
            console.log("Authorization failed: " + err);
        }
    }

    res.status(401);
    res.json({
        "status": 401,
        "message": "unknown userid, bye"
    });
});


// Restful login
router.post('/login', function (req, res) {

    var username = req.body.username || '';
    var password = req.body.password || '';

    // Do db lookup
    var loginName = req.app.get('username');
    var loginPass = req.app.get('password');

    // Check for empty body
    if (username == '' || password == '') {
        res.status(401);
        res.json({
            "status": 401,
            "message": "Unknown USER, bye"
        });
        return;
    }

    // Check for valid user/passwd combo
    if ((username == loginName) && (password == loginPass)) {
        var now = new Date();
        var expires = now.setHours(now.getDay() + 10);
        var token = jwt.encode({
            iss: username,
            exp: expires
        }, req.app.get('secretkey'));

        res.status(200);
        res.json({
            token: token,
            expires: expires,
            user: username
        });
    } else {
        res.status(401);
        res.json({
            "status": 401,
            "message": "Unknown USER, bye"
        });
    }
});


/**
 * Overzicht van alle murals
 */
router.get('/murals', function (req, res) {
    // Get Murals, Authors, Ratings
    var query = 'SELECT ' +
        'm.ID, m.published, m.date, m.dateModified, m.authorID, m.latitude, m.longitude, m.address, m.numberOnMap, m.videoUrl, m.year, m.photographer, m.videoAuthor, m.categoryID, a.name, r.rating ' +
        'FROM Murals m ' +
        'LEFT JOIN Authors a ON m.authorID = a.id ' +
        'LEFT JOIN (SELECT r.muralID, avg(r.rating) as rating FROM Ratings r GROUP BY r.muralID) r ON m.id = r.muralID' +
        ';';
    var dbfile = req.app.get('dbfile');
    var db = new sqlite3.Database(dbfile);
    waterfall([
        function (callback) {
            var results = [];
            db.all(query, function (err, rows) {
                if (err) {
                    return res.status(500).send("Mural query failed");
                }
                rows.forEach(function (item, i, list) {
                    var mural = {
                        id: item.ID,
                        published: item.published,
                        date: item.date,
                        dateModified: item.dateModified,
                        authorID: item.authorID,
                        latitude: item.latitude,
                        longitude: item.longitude,
                        address: item.address,
                        numberOnMap: item.numberOnMap,
                        videoUrl: item.videoUrl,
                        year: item.year,
                        photographer: item.photographer,
                        videoAuthor: item.videoAuthor,
                        author: item.name,
                        rating: item.rating,
                        title: {},
                        url: {},
                        description: {},
                        material: {},
                        category: {
                            id: item.categoryID,
                        },
                        images: [],
                    };
                    results.push(mural);
                });
                callback(null, results);
            });
        },
        function (results, callback) {
            // Get Language, MuralTranslations, Categories
            var localeQuery = 'SELECT ' +
                'mt.muralID, l.languageCode, mt.title, mt.url, mt.description, mt.material, ct.translation ' +
                'FROM Language l ' +
                'LEFT JOIN MuralTranslations mt ON mt.languageID = l.ID ' +
                'LEFT JOIN Murals m ON mt.muralID = m.ID ' +
                'LEFT JOIN Categories c ON c.ID = m.categoryID ' +
                'LEFT JOIN CategoryTranslations ct ON ct.categoryID = c.ID AND ct.languageID = l.id' +
                ';';
            var getMural = function (id) {
                var mural = null;
                results.forEach(function (item) {
                    if (item.id == id) {
                        mural = item;
                    }
                });
                return mural;
            };

            db.all(localeQuery, function (err2, localeRows) {
                if (err2) {
                    res.status(500);
                    res.json("Query failed for mural translations");
                }
                localeRows.forEach(function (locale) {
                    var mural = getMural(locale.muralID);
                    if (mural != null) {
                        var lang = locale.languageCode;
                        mural.title[lang] = locale.title;
                        mural.url[lang] = locale.url;
                        mural.description[lang] = locale.description;
                        mural.material[lang] = locale.material;
                        // TODO What if multiple categories? Is that even possible?
                        mural.category[lang] = locale.translation;
                    } else {
                        console.log("No mural for id: " + locale.muralID + " in results");
                    }
                });
                callback(null, results);
            });
        },
        function (results, callback) {
            // Get Images
            var imageQuery = 'SELECT * FROM Images';
            var getMural = function (id) {
                var mural = null;
                results.forEach(function (item) {
                    if (item.id == id) {
                        mural = item;
                    }
                });
                return mural;
            };

            db.all(imageQuery, function (err, imageRows) {
                imageRows.forEach(function (image) {
                    var mural = getMural(image.muralID);
                    mural.images.push({
                        id: image.id,
                        url: image.url,
                        file: String(image.file),
                        type: image.type,
                    });
                });
                callback(null, results);
            });
        }
    ], function (err, results) {
        db.close();
        res.status(200);
        res.json(results);
    });
});

/**
 * Overzicht van alle routes
 */
router.get('/routes', function (req, res) {
    // Get Routes, RouteTranslations
    var query = 'SELECT ' +
        'r.ID, r.time, r.distance, rt.name, l.languageCode, r.type ' +
        'FROM Routes r ' +
        'LEFT JOIN RouteTranslations rt ON r.ID = rt.routeID ' +
        'LEFT JOIN Language l ON rt.languageID = l.ID ' +
        ';';
    var dbfile = req.app.get('dbfile');
    var db = new sqlite3.Database(dbfile);
    function getRoute(results, id) {
        var route = null;
        results.forEach(function (item) {
            if (item.id == id) {
                route = item;
            }
        });
        return route;
    }
    waterfall([
        function (callback) {
            var results = [];
            db.all(query, function (err, rows) {
                if (err) {
                    return res.status(500).send("Routes query failed");
                }
                rows.forEach(function (item) {
                    var route = getRoute(results, item.ID);
                    if (route == null) {
                        route = {
                            id: item.ID,
                            time: item.time,
                            distance: item.distance,
                            type: item.type,
                            name: {},
                            points: [],
                        };
                        results.push(route);
                    }
                    var lang = item.languageCode;
                    route.name[lang] = item.name;
                });
                callback(null, results);
            });
        },
        function (results, callback) {
            // Get RoutePoints
            var localeQuery = 'SELECT ' +
                'rp.routeID, rp.ID, rp.muralID, rp.`order` ' +
                'FROM Routes r ' +
                'LEFT JOIN RoutePoints rp ON r.ID = rp.routeID ' +
                'ORDER BY r.ID, rp.`order`' +
                ';';

            db.all(localeQuery, function (err2, localeRows) {
                if (err2) {
                    res.status(500);
                    res.json("Query failed for RoutePoints");
                }
                localeRows.forEach(function (item) {
                    var route = getRoute(results, item.routeID);
                    if (route != null) {
                        route.points.push({
                            id: item.ID,
                            muralId: item.muralID,
                            order: item.order,
                        });
                    } else {
                        console.log("No route for id: " + item.routeID + " in results");
                    }
                });
                callback(null, results);
            });
        }
    ], function (err, results) {
        db.close();
        res.status(200);
        res.json(results);
    });
});

router.get('/comments', function(req, res) {
    var query = 'SELECT ' +
        'm.ID, m.numberOnMap, mt.title, r.rating, r.comment, r.date ' +
        'FROM Murals m ' +
        'LEFT JOIN MuralTranslations mt ON mt.muralID = m.ID ' +
        'LEFT JOIN Ratings r ON r.muralID = m.ID ' +
        'WHERE mt.languageID = 1 AND r.comment IS NOT NULL ' +
        'ORDER BY m.ID, r.ID' +
        ';';
    // console.log(query);
    var murals = [];
    var getMural = function (id) {
        var mural = null;
        murals.forEach(function (item) {
            if (item.id == id) {
                mural = item;
            }
        });
        return mural;
    };

    var dbfile = req.app.get('dbfile');
    var db = new sqlite3.Database(dbfile);
    db.all(query, function (err, rows) {
        if (err) {
            return res.status(500).send("Comment query failed");
        }
        rows.forEach(function (item) {
            var m = getMural(item.ID);
            if (m == null) {
                m = {
                    id: item.ID,
                    numberOnMap: item.numberOnMap,
                    title: item.title,
                    ratings: []
                };
                murals.push(m);
            }
            m.ratings.push({
                rating: item.rating,
                comment: item.comment,
                date: new Date(item.date).toISOString(),
            });
        });
        db.close();
        res.status(200);
        res.json(murals);
    });

});

/**
 * Post een rating voor een specifieke mural
 */
router.post('/rate', function (req, res) {
    var rating = req.body.rating || null;
    var muralId = req.body.muralId || null;
    var comment = req.body.comment || null;

    if (rating === null || muralId === null || isNaN(rating) || isNaN(muralId)) {
        res.status(400);
        res.json({
            "status": 400,
            "message": "Missing rating or mural id"
        });
        return;
    }
    if (rating < 0 || rating > 5 || muralId <= 0) {
        res.status(400);
        res.json({
            "status": 400,
            "message": "Rating or mural id out of bounds"
        });
        return;
    }

    var dbfile = req.app.get('dbfile');
    var db = new sqlite3.Database(dbfile);
    db.serialize(function () {
        var checkStmt = db.prepare('SELECT ID FROM Murals WHERE ID = ?');
        checkStmt.get(muralId, function (err, row) {
            if (row) {
                var stmt = db.prepare('INSERT INTO Ratings (rating, muralID, comment, date) VALUES (?, ?, ?, ?)');
                stmt.run(rating, muralId, comment, new Date().getTime());
                stmt.finalize();
                res.status(200);
                res.json({
                    "status": 200,
                    "message": "Rating added"
                });
            } else {
                res.status(400);
                res.json({
                    "status": 400,
                    "message": "Mural doesn't exist"
                });
            }
            db.close();
        });
        checkStmt.finalize();
    });
});

// Fall back, display some info
router.get('/', function (req, res) {
    res.status(200);
    res.json({
        "description": "Project Blindwall API version 2. Welcome"
    });
});

module.exports = router;
