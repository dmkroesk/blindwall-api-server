var express         = require('express');
var path            = require('path');
var bodyParser      = require('body-parser');
var fs              = require('fs');
var moment          = require('moment');
var routes_apiv1    = require('./routes_apiv1');
var routes_apiv2    = require('./routes_apiv2');
var app             = express();

// Read all app settings
try {
    var settings = require('./config.json');
} catch (err) {
    throw "Missing config.json, copy the config.json.example and check the settings.";
}
app.set('secretkey', settings.secretkey);
app.set('username', settings.username);
app.set('password', settings.password);
app.set('webPort', settings.webPort);
app.set('dbfile', path.join(__dirname, settings.database));

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Middelware, logging voor alle request
app.all('*', function (req, res, next) {
    // Log alle request
    console.log(req.method + " " + req.url);
    next();
});

// Middleware statische bestanden (HTML, CSS, images)
app.use('/static', express.static(__dirname + '/public'));

// Routing with versions
app.use('/apiv1', routes_apiv1);
app.use('/apiv2', routes_apiv2);


// Start server
var port = process.env.PORT || app.get('webPort');
var server = app.listen(port, function () {
    console.log('Listening server on port ' + server.address().port);
});
